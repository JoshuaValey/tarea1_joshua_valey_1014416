#include "pch.h"

using namespace System;
using namespace System::IO;
using namespace std;

static void Print(String^ a)
{
	Console::WriteLine(a);
}


static void PrintMenuOptions()
{
	Print("\nElija una de las siguientes Opciones:"
		+ "\n1. Multiplicaci�n de n�meros enteros."
		+ "\n2. Palabras Pal�ndromas."
		+ "\n3. Conversi�n de bases");
}

static bool ContinueInProgram() 
{
	bool resultado;

	char respuesta = ' ';
	do
	{
		Print("\nDesea continuar en el programa??? S/N \nIngrese su opcion y de doble enter:");
		respuesta = Convert::ToChar(Console::ReadLine());

	} while (respuesta != 'S' && respuesta != 'N');

	(respuesta == 'S') ? resultado = true : resultado = false;

	return resultado;
}

#pragma region Ejercicio 1: Calcular la multiplicaci�n de dos n�meros utilizando sumas 
//Forma Recursiva. 
int CalcularMultiplicidad(int n1, int n2)
{
	if (n2 == 0)
		return 0;
	else
		return CalcularMultiplicidad(n1, n2 - 1) + n1;
}
//Forma iterativa. 
static void CalcularMultiplicidadIterativa(int n1, int n2)
{
	int acumulador = 0;
	for (int i = 0; i < n2; i++)
		acumulador += n1;

	Console::WriteLine("la respuesta a la multiplicacion de"+ n1 + "con" + n2+ " es: " + acumulador);
}
#pragma endregion

#pragma region Ejercicio 2: Comprobaci�n de palabras pal�ndromas 
//Forma Recursiva. 
static bool PalabraPalindroma(String ^ palabra)
{
	if (palabra->Length == 0)
		return false;
	if (palabra->Length < 2)
		return true;

	if (palabra[0] == palabra[palabra->Length - 1])
		return PalabraPalindroma(palabra->Substring(1, palabra->Length - 2));
	else
		return false;

}
//Forma iterativa. 
static void PalabraPalindromaIterativa(String^ palabra)
{
	String^ newPalabra;
	for (int i = palabra->Length - 1; i >= 0; i--)
		newPalabra += palabra[i];

	(newPalabra == palabra) ?
		Print("La palabra es palindroma!!\n")
		: Print("La palabra no es palindroma!!\n");

}
#pragma endregion

#pragma region Ejercicio 3: Conversi�n de base m a base n

static bool EsBinario(String^ numero)
{
	bool resultado;
	for (int i = 0; i < numero->Length; i++)
	{
		(numero[i] != '0' && numero[i] != '1') ?
			resultado = false : resultado = true;
	}

	return resultado;
}

//Forma Recursiva. 
String^ ABaseDecimal(int n, int m, int numero)
{
	String^ numeroS = Convert::ToString(numero);
	int exponente;
	int numeroSLength = numeroS->Length;
	int acumuladorDecimal;


	if (!(n == 2) && !EsBinario(Convert::ToString(numero)))
		return "000";
	else if(n == 10)
	{
		return numeroS;
	}
	else
	{
		
		if (numero > 9)
		{
			exponente++;
			int lastNumber = Convert::ToInt32(numeroS[numeroSLength - 1]);
			acumuladorDecimal += lastNumber ^ (exponente - 1);
			return ABaseDecimal(n, m, Convert::ToInt32(numeroS->Substring(0, numeroSLength - 2)));
		}
		else
			return "1";
		
	}

}

String^ DecimalToBaseM(int decimal, int m)
{
	if (decimal == 0)
		return "";
	else
		return DecimalToBaseM(decimal / m, m) + (decimal % m);
}

//Forma iterativa. 
void ConversionDeBaseIterativa()
{

}
#pragma endregion

int main(array<System::String^>^ args)
{
	Print("Tarea No. 1 Recursividad");


	bool user_option_mistake_exception = true;

	PrintMenuOptions();
	while (user_option_mistake_exception)
	{
		try
		{
			int opcion = Convert::ToInt16(Console::ReadLine());
			switch (opcion)
			{
			case 1: {
				Console::Clear();


				Print("Ingrese el primer n�mero a operar");
				int operador1 = Convert::ToInt32(Console::ReadLine());

				Print("Ingrese el segundo n�mero a operar");
				int operador2 = Convert::ToInt32(Console::ReadLine());

				Print("Multiplicidad Por Recursividad");

				Print("La respuesta a la multiplicaci�n es: "
					+ CalcularMultiplicidad(operador1, operador2));

				Print("Multiplicidad Por Iteraci�n");

				CalcularMultiplicidadIterativa(operador1, operador2);


				user_option_mistake_exception = ContinueInProgram();
			}
				break;

			case 2: {

				Console::Clear();

				Print("Ingrese por favor una palabra que desee verificar si es palindroma: "
					+ "\nIngrese solo lentras minusculas");

				String^ palabra = Console::ReadLine();

				Print("Palindroma Por recursividad");
				(PalabraPalindroma(palabra) ?
					Print("palabra es palindroma") : Print("No es palindroma "));


				Print("Palindroma por Iteracion");
				PalabraPalindromaIterativa(palabra);

				user_option_mistake_exception = ContinueInProgram();
			}
				break;
			case 3:
			{
				Console::Clear();

				Print("Conversion de base n a m Recursiva");

				Print("Ingrese el valor de la base de su n�mero");
				int baseInicial = Convert::ToInt32(Console::ReadLine());

				Print("Ingrese el valor de la base a la que quiere convertir");
				int baseDestino = Convert::ToInt32(Console::ReadLine());

				Print("Ingrese su n�mero a convertir");
				int numero = Convert::ToInt32(Console::ReadLine());

				DecimalToBaseM(Convert::ToInt32(ABaseDecimal(baseInicial,baseDestino,numero)),baseDestino);

				Print("Conversion de base n a m Iterativa");
				Print("No ha sido implementada");


				user_option_mistake_exception = ContinueInProgram();
			}
				break;
			default:
				Console::Clear();
				Print("Opci�n incorrecta");
				PrintMenuOptions();

				break;
			}
		}
		catch (FormatException^ )
		{
			Console::Clear();
			Print("Ha ingresado un valor incorrecto, intente de nuevo...\n");
			PrintMenuOptions();
		}

	}

	Console::ReadLine();
	return 0;
}










